<?php

namespace Allegro;

class Allegro
{
	public static $soapClient;
	
	public static $webapiKey;
	private static $login;
	private static $password;
	
	private static $localversion;
	
	private static $sessionId;
	
//	private $user_id;
	private $filterOptions;
	private $sortOptions;
	
	
	public static function init( $webapiKey, $login, $password, $test = false )
	{
		if( empty( self::$localversion ) )
		{
			if ( $test )
			{
				self::$soapClient = new \SoapClient('https://webapi.allegro.pl.webapisandbox.pl/service.php?wsdl'); 
			}
			else
			{
				self::$soapClient = new \SoapClient('https://webapi.allegro.pl/service.php?wsdl');
			}
					
			self::$webapiKey = $webapiKey;
			self::$login = $login;
			self::$password = $password;
			
			$doquerysysstatus_request = array(
				   'sysvar' => 3,
				   'countryId' => 1,
				   'webapiKey' => self::$webapiKey,
				);
				
			$version = self::$soapClient->doQuerySysStatus($doquerysysstatus_request);
				
			self::$localversion = $version->verKey;
		}
	}
	
	public static function connect()
	{
		$dologin_request = array(
				'userLogin' => self::$login,
				'userHashPassword' => base64_encode( mhash(MHASH_SHA256, self::$password ) ),
				'countryCode' => 1,
				'webapiKey' => self::$webapiKey,
				'localVersion' => self::$localversion,
					);	
				
			$login = self::$soapClient->doLoginEnc($dologin_request); 
			
			self::$sessionId = $login->sessionHandlePart;		
	}
	
	public static function is_login( $_login )
	{
		$dogetuserid_request = array(
		   'countryId' => 1,
		   'userLogin' => $_login,
		   'userEmail' => '',
		   'webapiKey' => Allegro::$webapiKey
		);
		 
		try {
			$result = self::$soapClient->doGetUserID( $dogetuserid_request ); 	
		}
		catch( \SoapFault $e )
		{
			return false;
		}

		return $result->userId;
	}
	
	public function __construct( $user )
	{
		$dogetuserid_request = array(
		   'countryId' => 1,
		   'userLogin' => $user,
		   'userEmail' => '',
		   'webapiKey' => Allegro::$webapiKey
		);
		 
		$result = Allegro::$soapClient->doGetUserID( $dogetuserid_request ); 
		
	//	$this->user_id = $result->userId;
		$this->filterOptions = array();
		$this->sortOptions = array();
		
		$this->add_filter('userId', $result->userId);
	}	
	
	public function znajdz_aukcje( $ile )
	{
		$dogetitemslist_request = array(
			'webapiKey' => Allegro::$webapiKey,
			'countryId' => 1,
			'filterOptions' => $this->filterOptions,
			'sortOptions' => $this->sortOptions,
			'resultSize' => $ile,
			'resultOffset' => 0,
			'resultScope' => 3 //0 - jesli chcemy pokazac opcje filtrowania w rezultatach
		);
		
		return $result = Allegro::$soapClient->doGetItemsList( $dogetitemslist_request );		
	}
	
	public function add_range_filter( $id, $min, $max )
	{
		$array = array(
			'filterId' => $id,
			'filterValueId' => '',
			'filterValueRange' => array(
							'rangeValueMin' => $min,
							'rangeValueMax' => $max
							));				
		
		array_push( $this->filterOptions, $array );
	}
	
	public function add_filter( $id, $value )
	{
		$array = array(
			'filterId' => $id,
			'filterValueId' => array( $value ));
		
		array_push( $this->filterOptions, $array );
	}
	
	public function add_sort_options( $sortType, $sortOrder )
	{
		$this->sortOptions = array(
			'sortType' => $sortType,
			'sortOrder' => $sortOrder);
	}
}

?>
